# Things left to do
## Beautification
- [] About Page
## Golang
- [] HandleNCX
    => function {
        - if item.URL is not null {
                format to a href format
                add to build string
            }
        - if item has children
            move in
            continue
        - if item is last and has parent
            move out
            continue
        - if item is last and has no parent
            break
        - send buildstring to replaceA
        - write to TOC
    }
- [] Error page to show if port is in use/conversion failed. Need Template parsing
- [] Template parsing of static pages
## TW5
- Make Action-Mark and Clevernote a Plugin
- ActionMark is having errors when superscript is involved