package main

import (
	"bytes"
	"io/ioutil"
	"log"
	"os"
	"testing"

	"gitlab.com/ibnishak/epubgo"
)

// func TestReplaceImg(t *testing.T) {
// 	os := `<img class="myclass" src="../images/new.jpg">`
// 	result := replaceImg(os)
// 	expected := `<$image class="myclass"  source="new.jpg">`
// 	if result != expected {
// 		t.Errorf("Replace Img: Incorrect output: CURRENT %s, EXPECTED: %s", result, expected)
// 	}
// }

var imglinks = []struct {
	given string
	want  string
}{
	{`<img src="../images/now.jpg">`, `<$image source="now.jpg"/>`},
	{`<img class="my class" src="../images/now.jpg">`, `<$image source="now.jpg"/>`},
	{`<img class="ho" src="now.jpg" style="bg:no" id="h">`, `<$image source="now.jpg"/>`},
}

func TestReplaceImg(t *testing.T) {
	for _, value := range imglinks {
		t.Run(value.given, func(t *testing.T) {
			got := replaceImg(value.given)
			if got != value.want {
				t.Errorf("\nGOT: %q\n\nWNT: %q", got, value.want)
			}
		})
	}
}

func TestConvertProper(t *testing.T) {
	r, err := os.Open("ungit/toc-as-nav.epub")
	if err != nil {
		log.Fatal(err)
	}
	defer r.Close()
	b, err := ioutil.ReadAll(r) // The readCloser is the one from the zip-package
	if err != nil {
		panic(err)
	}

	// bytes.Reader implements io.Reader, io.ReaderAt, etc. All you need!
	readerAt := bytes.NewReader(b)
	_, err = convert(readerAt, 4753022)
	if err != nil {
		t.Errorf("%s", err.Error())
	}
}

func TestReadReference(t *testing.T) {
	file, err := epubgo.Open("ungit/toc-in-reference.epub")
	if err != nil {
		log.Println("", err.Error())
	}
	got := readReference(file)
	want := "text/part0004.html"
	if got != want {
		t.Errorf("\n\nGOT: %s\nWANT: %s", got, want)
	}
}

var links = []struct {
	given string
	want  string
}{
	{`<a href="intro.html#a1">With ID</a><br/>`, `<$link scroll="no" to="intro.html" anchor="a1">With ID</$link><br/>`},
	{`<a href="../folder/intro.html#a1">With ID</a><br/>`, `<$link scroll="no" to="intro.html" anchor="a1">With ID</$link><br/>`},
	{`<a href="text/part0000.html">No ID</a>`, `<$link scroll="no" to="part0000.html">No ID</$link>`},
	{`<a class="myclass" href="intro.html#a1" style="backgound:blue">With Class and other things</a><br/>`, `<$link scroll="no" to="intro.html" anchor="a1">With Class and other things</$link><br/>`},
	{`<sup class="calibre10"><a href="part0035_split_008.html#ich01note1" class="nounder">1</a></sup>`, `<sup class="calibre10"><$link scroll="no" to="part0035_split_008.html" anchor="ich01note1">1</$link></sup>`},
}

func TestReplaceA(t *testing.T) {

	for _, value := range links {
		t.Run(value.given, func(t *testing.T) {
			got := replaceA(value.given)
			if got != value.want {
				t.Errorf("\nGOT: %q\n\n WNT: %q", got, value.want)
			}
		})
	}
}

var books = []struct {
	book string
	size int64
}{
	{"ungit/toc-in-reference.epub", 489293},
	{"ungit/toc-as-nav.epub", 4753022},
	{"ungit/mueller.epub", 4390609},
}

func TestConvertTable(t *testing.T) {

	for _, value := range books {
		t.Run(value.book, func(t *testing.T) {
			r, err := os.Open(value.book)
			if err != nil {
				log.Fatal(err)
			}
			defer r.Close()
			b, err := ioutil.ReadAll(r)
			if err != nil {
				panic(err)
			}

			readerAt := bytes.NewReader(b)
			_, err = convert(readerAt, value.size)
			if err != nil {
				t.Errorf("%s", err.Error())
			}
		})
	}
}

func BenchmarkConvert3(b *testing.B) {
	r, err := os.Open("ungit/toc-as-nav.epub")
	if err != nil {
		log.Fatal(err)
	}
	defer r.Close()
	a, err := ioutil.ReadAll(r) // The readCloser is the one from the zip-package
	if err != nil {
		panic(err)
	}

	// bytes.Reader implements io.Reader, io.ReaderAt, etc. All you need!
	readerAt := bytes.NewReader(a)
	b.Run("BenchConvert", func(b *testing.B) {
		for n := 0; n < b.N; n++ {
			//Function here
			convert(readerAt, 4753022)
		}
	})
}
