package main

import (
	"bytes"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"path/filepath"
)

func uploadFile(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodGet {
		tmpl, err := template.ParseFiles("static/index.html")
		if err != nil {
			log.Println(err.Error())
		}

		err = tmpl.Execute(w, nil)
		if err != nil {
			log.Println(err.Error())
		}
	} else {
		err := r.ParseMultipartForm(10 << 20)
		if err != nil {
			log.Println("", err.Error())
		}
		clevernote := r.FormValue("clevernote")
		file, handler, err := r.FormFile("epubfile")
		if err != nil {
			log.Println("Error retrieving file:", err.Error())
		}
		defer file.Close()

		fileBytes, err := ioutil.ReadAll(file)
		if err != nil {
			log.Println("Error reading file:", err.Error())
		}
		epub := bytes.NewReader(fileBytes)
		TWF, err := convert(epub, handler.Size)
		if err != nil {
			log.Println(err.Error())
		}
		tgt, err := moveBook(TWF)
		if err != nil {
			log.Println("", err.Error())
		}
		copyHelpers(clevernote, filepath.Join(tgt, "tiddlers"))
		tmpl, err := template.ParseFiles("static/success.html")
		if err != nil {
			log.Println(err.Error())
		}

		err = tmpl.Execute(w, nil)
		if err != nil {
			log.Println(err.Error())
		}
	}

}
