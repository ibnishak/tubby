package main

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"sync"

	"github.com/kennygrant/sanitize"
	"github.com/mitchellh/go-homedir"
	co "github.com/otiai10/copy"
	"github.com/skratchdot/open-golang/open"
	"gitlab.com/ibnishak/epubgo"
	"golang.org/x/net/html"
)

var wg sync.WaitGroup

func convert(r io.ReaderAt, s int64) (string, error) {
	//log.Println("Starting conversion")
	//log.Printf("Size of file: %d", s)
	f, err := epubgo.Load(r, s)
	if err != nil {
		return "", err //Important Error
	}
	defer f.Close()
	TWF, TIDF, err := metaTiddlers(f)
	if err != nil {
		log.Println("", err.Error())
	}
	var ncx bool = true
	for _, item := range f.Opf.Manifest {
		if strings.Contains(item.Properties, "nav") || strings.Contains(item.ID, "toc") {
			wg.Add(1)
			go func() {
				ncx = false
				body, err := handleHTML(f, item.Href)
				if err != nil {
					log.Println("", err.Error())
				}
				err = writetid("TOC", "", "", "", "", body, TIDF)
				if err != nil {
					log.Println("", err.Error())
				}
				wg.Done()
			}()
			continue
		}
		if strings.Contains(item.MediaType, "html") {
			wg.Add(1)
			go func() {
				body, err := handleHTML(f, item.Href)
				if err != nil {
					log.Println("", err.Error())
				}
				body = replaceImg(body)
				err = writetid(filepath.Base(item.Href), "Contents", "", "", "", body, TIDF)
				if err != nil {
					log.Println("", err.Error())
				}
				wg.Done()
			}()
			continue
		}
		if strings.Contains(item.MediaType, "image") {
			wg.Add(1)
			go func() {
				i, err := f.OpenFile(item.Href)
				if err != nil {
					log.Println("Error in opening image file", err.Error())
				}
				defer i.Close()
				cssFile := filepath.Base(item.Href)
				outFile, err := os.Create(filepath.Join(TIDF, cssFile))
				if err != nil {
					log.Println("", err.Error())
				}
				defer outFile.Close()
				_, err = io.Copy(outFile, i)
				if err != nil {
					log.Println("", err.Error())
				}
				b := fmt.Sprintf("title: %s\ntags: Images\ntype: %s\n", cssFile, item.MediaType)
				err = ioutil.WriteFile(filepath.Join(TIDF, fmt.Sprintf("%s.meta", cssFile)), []byte(b), 0644)
				if err != nil {
					log.Println("", err.Error())
				}
				wg.Done()
			}()
			continue
		}

		if strings.Contains(item.MediaType, "css") {
			wg.Add(1)
			go func() {
				i, err := f.OpenFile(item.Href)
				if err != nil {
					log.Println("", err.Error())
				}
				defer i.Close()
				outFile, err := os.Create(filepath.Join(TIDF, item.ID))
				if err != nil {
					log.Println("", err.Error())
				}
				defer outFile.Close()
				_, err = io.Copy(outFile, i)
				if err != nil {
					log.Println("", err.Error())
				}
				b := fmt.Sprintf("title: %s\ntags: $:/tags/Stylesheet\ntype: %s\n", item.ID, item.MediaType)
				err = ioutil.WriteFile(filepath.Join(TIDF, fmt.Sprintf("%s.meta", item.ID)), []byte(b), 0644)
				if err != nil {
					log.Println("", err.Error())
				}
				wg.Done()
			}()
			continue
		}
	}
	wg.Wait()
	if ncx {
		nav := readReference(f)
		body, err := handleHTML(f, nav)
		if err != nil {
			log.Println("", err.Error())
		}
		err = writetid("TOC", "", "", "", "", body, TIDF)
		if err != nil {
			return "", err
		}
	}
	return TWF, nil
}
func moveBook(TWF string) (tgt string, err error) {
	home, err := homedir.Dir()
	if err != nil {
		return "", err
	}
	tgt = filepath.Join(home, "Documents", TWF)
	err = os.MkdirAll(tgt, 0777)
	if err != nil {
		return "", err
	}
	err = os.Rename(TWF, tgt)
	if err != nil {
		err = open.Start(TWF)
		if err != nil {
			return "", err
		}
		return "", err
	}
	err = open.Start(tgt)
	if err != nil {
		return "", err
	}
	return tgt, nil
}
func handleHTML(f *epubgo.Epub, path string) (string, error) {
	i, err := f.OpenFile(path)
	if err != nil {
		log.Println("", err.Error())
	}
	defer i.Close()
	doc, _ := html.Parse(i)
	bn, err := getBody(doc)
	if err != nil {
		// Important error
		return "", err
	}
	body := renderNode(bn)
	body = replaceA(body)

	return body, nil
}

func readReference(file *epubgo.Epub) string {
	var nav string
	for _, item := range file.Opf.Guide {
		if item.Type == "toc" {
			nav = item.Href
		}
	}
	return nav
}

func copyHelpers(clevernote string, path string) {
	if clevernote == "clevernote" {
		err := co.Copy("helpers/clevernote", path)
		if err != nil {
			log.Println("", err.Error())
		}
	} else {
		err := co.Copy("helpers/defaults", path)
		if err != nil {
			log.Println("", err.Error())
		}
	}
}

func writetid(title, tags, ttype, caption, fields, body, path string) error {
	b := fmt.Sprintf("title: %s\ntags: %s\ntype: %s\ncaption: %s\n%s\n\n%s", title, tags, ttype, caption, fields, body)
	err := ioutil.WriteFile(filepath.Join(path, sanitize.Name(fmt.Sprintf("%s.tid", title))), []byte(b), 0644)
	if err != nil {
		return err
	}
	return nil
}

func rmmk(p string) {
	err := os.RemoveAll(p)
	if err != nil {
		log.Println("Error in removing folder ", p, err.Error())
	}

	err = os.MkdirAll(p, 0777)
	if err != nil {
		log.Println("Error in creating folder", p)
	}
}

func getBody(doc *html.Node) (*html.Node, error) {
	var b *html.Node
	var f func(*html.Node)
	f = func(n *html.Node) {
		if n.Type == html.ElementNode && n.Data == "body" {
			b = n
		}
		for c := n.FirstChild; c != nil; c = c.NextSibling {
			f(c)
		}
	}
	f(doc)
	if b != nil {
		return b, nil
	}
	return nil, errors.New("malformed html")
}

func renderNode(n *html.Node) string {
	var buf bytes.Buffer
	w := io.Writer(&buf)
	err := html.Render(w, n)
	if err != nil {
		log.Println("", err.Error())
	}
	return buf.String()
}

func replaceImg(oldstring string) string {
	re := regexp.MustCompile(`<img[^>]*?src="(.*?)".*?>`)
	res := re.FindAllStringSubmatch(oldstring, -1)
	for i := range res {
		var replacer string
		url := filepath.Base(res[i][1])
		replacer = fmt.Sprintf(`<$image source="%s"/>`, url)
		oldstring = strings.Replace(oldstring, res[i][0], replacer, -1)
	}
	return oldstring
}

func replaceA(oldstring string) string {

	regex := regexp.MustCompile(`<a[^>]*?href="(.*?)".*?>(.*?)</a>`)
	res := regex.FindAllStringSubmatch(oldstring, -1)

	for i := range res {
		var replacer string
		if strings.Contains(res[i][1], "#") {
			anchor := strings.Split(res[i][1], "#")
			url := filepath.Base(anchor[0])
			replacer = fmt.Sprintf(`<$link scroll="no" to="%s" anchor="%s">%s</$link>`, url, anchor[1], res[i][2])
		} else {
			url := filepath.Base(res[i][1])
			replacer = fmt.Sprintf(`<$link scroll="no" to="%s">%s</$link>`, url, res[i][2])
		}
		oldstring = strings.Replace(oldstring, res[i][0], replacer, -1)
	}
	return oldstring
}

func metaTiddlers(f *epubgo.Epub) (string, string, error) {
	title, err := f.Metadata("title")
	if err != nil {
		return "", "", err
	}
	TWF := sanitize.Name(title[0])
	TIDF := filepath.Join(TWF, "tiddlers")
	rmmk(TIDF)

	err = writetid("$:/SiteTitle", "", "", "", "", title[0], TIDF)
	if err != nil {
		log.Println("", err.Error())
	}
	// Find Author and Write Author tiddler
	author, _ := f.Metadata("creator")
	err = writetid("Author", "Metadata", "", "", "", author[0], TIDF)
	if err != nil {
		log.Println("", err.Error())
	}
	err = co.Copy("helpers/tiddlywiki.info", filepath.Join(TWF, "tiddlywiki.info"))
	if err != nil {
		log.Println("", err.Error())
	}
	return TWF, TIDF, nil
}
