module gitlab.com/ibnishak/tubby

go 1.13

require (
	github.com/kennygrant/sanitize v1.2.4
	github.com/mitchellh/go-homedir v1.1.0
	github.com/otiai10/copy v1.0.2
	github.com/skratchdot/open-golang v0.0.0-20190402232053-79abb63cd66e
	gitlab.com/ibnishak/epubgo v0.0.0-20190924021630-88f1358f3262
	golang.org/x/net v0.0.0-20190923162816-aa69164e4478
)
