package main

import (
	"context"
	"log"
	"net"
	"net/http"

	"github.com/skratchdot/open-golang/open"
)

func setupRoutes() {
	srv := http.Server{}
	fs := http.FileServer(http.Dir("static"))
	http.Handle("/rohi/", http.StripPrefix("/rohi/", fs))
	http.HandleFunc("/rohi/upload", uploadFile)
	http.HandleFunc("/rohi/shutdown", func(w http.ResponseWriter, r *http.Request) {
		err := srv.Shutdown(context.Background())
		if err != nil {
			log.Println("", err.Error())
		}
	})
	log.Println("Listening at http://localhost:8083/rohi/")

	l, err := net.Listen("tcp", "localhost:8083")
	if err != nil {
		log.Fatal(err)
	}
	err = open.Start("http://localhost:8083/rohi/upload")
	if err != nil {
		panic(err)
	}
	log.Fatal(srv.Serve(l))

	log.Printf("Rohi server shutdown")
}
